package jp.co.nec.flowlites.usercode;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class USER_SINSEI implements UserCodeSinsei {

    public void beforeInitSinsei(FunctionFlgs functionFlgs, UserInfo user, SinseiData sinseiData, Map params, Map tools)
            throws Exception {

        System.out.println("USER_SINSEI beforeInitSinsei");

    }

    public void afterInitSinsei(FunctionFlgs functionFlgs, UserInfo user, SinseiData sinseiData, Map params, Map tools)
            throws Exception {
        System.out.println("USER_SINSEI afterInitSinsei");

    }

    public boolean checkGyo(FunctionFlgs functionFlgs, UserInfo user, SinseiData sinseiData, Map params, Map tools)
            throws Exception {
        System.out.println("USER_SINSEI checkGyo");

        return false;
    }

    public void beforeCheckSinseiData(FunctionFlgs functionFlgs, UserInfo user, SinseiData sinseiData, Map params, Map tools)
            throws Exception {
        System.out.println("USER_SINSEI beforeCheckSinseiData");

    }

    public void afterCheckSinseiData(FunctionFlgs functionFlgs, UserInfo user, SinseiData sinseiData, Map params, Map tools)
            throws Exception {
        System.out.println("USER_SINSEI afterCheckSinseiData");

    }

    public void routeUpdate(FunctionFlgs functionFlgs, UserInfo user, SinseiData sinseiData, RouteData routeData, Map params, Map tools)
            throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date) + " USER_SINSEI routeUpdate " + user.getKanaSimei());

    }

    public void routeCheck(FunctionFlgs functionFlgs, UserInfo user, SinseiData sinseiData, RouteData routeData, Map params, Map tools)
            throws Exception {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date) + " USER_SINSEI routeCheck " + user.getKanaSimei());
    }

    public void beforeSaveSinsei(FunctionFlgs functionFlgs, UserInfo user, SinseiData sinseiData, Map params, Map tools)
            throws Exception {
        System.out.println("ASH beforeSaveSinsei : ");

    }

    public void afterSaveSinsei(FunctionFlgs functionFlgs, UserInfo user, SinseiData sinseiData, Map params, Map tools)
            throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date) + " USER_SINSEI afterSaveSinsei " + user.getKanaSimei());
    }

    public void beforeSinsei(FunctionFlgs functionFlgs, UserInfo user, SinseiData sinseiData, RouteData routeData, Map params, Map tools)
            throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date) + " USER_SINSEI beforeSinsei " + user.getKanaSimei());
    }

    public void afterSinsei(FunctionFlgs functionFlgs, UserInfo user, SinseiData sinseiData, RouteData routeData, Map params, Map tools)
            throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date) + " USER_SINSEI afterSinsei " + user.getKanaSimei());

    }

    public void torikesi(FunctionFlgs functionFlgs, UserInfo user, SinseiData sinseiData, Map params, Map tools)
            throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date) + " USER_SINSEI torikesi " + user.getKanaSimei());
    }

}
