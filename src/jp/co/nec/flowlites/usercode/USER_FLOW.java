package jp.co.nec.flowlites.usercode;

import java.util.Map;

public class USER_FLOW implements UserCodeFlow {

    public void onCompletedStepForRouteUpdate(int stepNo, FunctionFlgs functionFlgs, UserInfo user,
            SinseiData sinseiData, RouteData routeData, Map params, Map tools)
            throws Exception {
        System.out.println("USER_FLOW onCompletedStepForRouteUpdate");

    }

    public void onBeforeCompletedStep(int stepNo, FunctionFlgs functionFlgs, UserInfo user,
            SinseiData sinseiData, RouteData routeData, Map params, Map tools)
            throws Exception {
        System.out.println("USER_FLOW onBeforeCompletedStep");

    }

    public void onCompletedStep(int stepNo, FunctionFlgs functionFlgs, UserInfo user,
            SinseiData sinseiData, RouteData routeData, Map params, Map tools)
            throws Exception {
        System.out.println("USER_FLOW onCompletedStep");

    }

    public void onCompletedFlowForRouteUpdate(FunctionFlgs functionFlgs, UserInfo user,
            SinseiData sinseiData, RouteData routeData, Map params, Map tools)
            throws Exception {
        System.out.println("USER_FLOW onCompletedFlowForRouteUpdate");

    }

    public void onBeforeCompletedFlow(FunctionFlgs functionFlgs, UserInfo user,
            SinseiData sinseiData, RouteData routeData, Map params, Map tools)
            throws Exception {
        System.out.println("USER_FLOW onBeforeCompletedFlow");

    }

    public void onCompletedFlow(FunctionFlgs functionFlgs, UserInfo user,
            SinseiData sinseiData, RouteData routeData, Map params, Map tools)
            throws Exception {
        System.out.println("USER_FLOW onCompletedFlow");

    }

}
