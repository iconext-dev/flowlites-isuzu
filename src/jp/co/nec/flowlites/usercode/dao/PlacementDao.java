package jp.co.nec.flowlites.usercode.dao;

import java.util.List;
import java.util.Map;
import jp.co.nec.flowlites.usercode.FlDb;
import jp.co.nec.flowlites.entity.SosikiEntity;

public class PlacementDao {

    public SosikiEntity getByEmp(Map tools,String empCode) {
        System.out.println("PlacementDao->getByEmp");
        System.out.println("empCode : " + empCode);
        SosikiEntity placement = new SosikiEntity();
        FlDb db = (FlDb) (tools.get("db"));
        StringBuffer sql = null;
        sql = new StringBuffer();
        sql.append("SELECT TM_SOSIKI.* FROM TM_SYOZOKU LEFT JOIN TM_SOSIKI ON TM_SYOZOKU.SOSIKI_CODE=TM_SOSIKI.SOSIKI_CODE");
        sql.append(" WHERE SYAIN_NO=?");
        List list = db.queryForList(sql.toString(),empCode);
        Map map = (Map) list.get(0);
        placement.setSOSIKI_CODE(map.get("SOSIKI_CODE").toString());
        placement.setSOSIKI_NAME(map.get("SOSIKI_NAME").toString());
       
        return placement;
    }


}
