package jp.co.nec.flowlites.usercode.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import jp.co.nec.flowlites.entity.SyainBaseEntity;
import jp.co.nec.flowlites.entity.HanyoEntity;
import jp.co.nec.flowlites.usercode.FlDb;

public class RouteDao {

    public List<SyainBaseEntity> getEmployee(Map tools, String group,String company) {
        System.out.println("RouteDao->getEmployee");
        System.out.println("groupCode : " + group);
        List<SyainBaseEntity> employees = new ArrayList<SyainBaseEntity>();
        FlDb db = (FlDb) (tools.get("db"));
        StringBuffer sql = null;
        sql = new StringBuffer();
        sql.append("SELECT * FROM TM_HANYO");
        sql.append(" WHERE CHR3=? AND CHR2=?");
        List list = db.queryForList(sql.toString(), group,company);
        for (int i = 0; i < list.size(); i++) {
            Map map = (Map) list.get(i);
            SyainBaseEntity emp = new SyainBaseEntity();
            emp.setSYAIN_NO(map.get("CHR5").toString());
            emp.setSYAIN_BETUMEI(map.get("CHR4").toString());
            System.out.println("SYAIN_NO : " + emp.getSYAIN_NO());
            
            employees.add(emp);
        }

        return employees;
    }
    
    public List<HanyoEntity> getRoute(Map tools, String appNo,int stepNo) {
        System.out.println("RouteDao->getRoute");
        List<HanyoEntity> route = new ArrayList<HanyoEntity>();
        FlDb db = (FlDb) (tools.get("db"));
        StringBuffer sql = null;
        sql = new StringBuffer();
        sql.append("SELECT * FROM TM_HANYO");
        sql.append(" WHERE HANYO_KBN='isuzu_route' AND CHR2='1' AND CHR1=? AND NUM1=?");
        List list = db.queryForList(sql.toString(), appNo,stepNo);
        for (int i = 0; i < list.size(); i++) {
            Map map = (Map) list.get(i);
            HanyoEntity emp = new HanyoEntity();
            emp.setCHR1(map.get("CHR1").toString());
            emp.setCHR2(map.get("NUM1").toString());
            route.add(emp);
        }

        return route;
    }
    
    

}
