package jp.co.nec.flowlites.usercode.dao;

import java.util.List;
import java.util.Map;
import jp.co.nec.flowlites.usercode.FlDb;
import jp.co.nec.flowlites.entity.SyainBaseEntity;

public class EmployeeDao {

    public SyainBaseEntity getById(Map tools, String empCode) {
        System.out.println("EmployeeDao->getById");
        System.out.println("empCode : " + empCode);
        SyainBaseEntity employee = new SyainBaseEntity();
        FlDb db = (FlDb) (tools.get("db"));
        StringBuffer sql = null;
        sql = new StringBuffer();
        sql.append("SELECT * FROM TM_SYAIN_BASE");
        sql.append(" WHERE SYAIN_NO=?");
        List list = db.queryForList(sql.toString(), empCode);

        Map map = (Map) list.get(0);
        employee.setSYAIN_NO(map.get("SYAIN_NO").toString());
        employee.setSIMEI_SEI(map.get("SIMEI_SEI").toString());
        employee.setSIMEI_NA(map.get("SIMEI_NA").toString());
        employee.setSYAIN_BETUMEI(map.get("SYAIN_BETUMEI").toString());

        System.out.println("SYAIN_NO : " + employee.getSYAIN_NO() + " SIMEI_SEI " + employee.getSIMEI_SEI() + "SIMEI_NA " + employee.getSIMEI_NA());

        return employee;
    }

}
