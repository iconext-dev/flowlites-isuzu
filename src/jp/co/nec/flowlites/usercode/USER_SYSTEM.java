package jp.co.nec.flowlites.usercode;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
* システム処理
*/
public class USER_SYSTEM implements UserCodeSystem {
    
    public void afterAutoDeleteSinseiData( SinseiData sinseiData, 
            Map params, Map tool ) throws Exception {
        
         DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date) + " USER_SYSTEM afterAutoDeleteSinseiData " + sinseiData.getHead().getDaikoHakkosyaName());
        
    }

    public void afterAutoDeleteRirekiSinseiData( SinseiData sinseiData, 
            Map params, Map tool ) throws Exception {
         DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date) + " USER_SYSTEM afterAutoDeleteRirekiSinseiData " + sinseiData.getHead().getDaikoHakkosyaName());
        
        
    }

}
