package jp.co.nec.flowlites.usercode.sap;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import jp.co.nec.flowlites.core.util.FLPropertiesUtils;
import org.springframework.util.ResourceUtils;

public final class PropertiesFile {

    private static final String PROPERTY_FILE = "classpath:resource/config/SapProp.properties";
    private static final String DATABASE_CONNECTTION = "database.connection";
    private static final String APP_CODE = "app.code";
    private static final String SETTING_COMCODE = "setting.comcode";
    private static final String SETTING_BUSAREA = "setting.busarea";
    private static final String SETTING_HEADERTEXT = "setting.headertext";
    private static final String SETTING_DUMMYCODE1 = "setting.dummycode1";
    private static final String SETTING_DUMMYCODE2 = "setting.dummycode2";
    private static final String CSV_ITEM_PATH = "input.csv.item";
    private static final String CSV_OUTPUT_PATH = "output.csv.path";

    private String dbConnect;
    private String appCode;
    private String settingComcode;
    private String settingBusarea;
    private String csvItem;
    private String csvOutputPath;
    private String headerText;
    private String dummycode1;
    private String dummycode2;

    public PropertiesFile() throws IOException {

        try {
            URL propsUrl = ResourceUtils.getURL(PROPERTY_FILE);
            Properties config = FLPropertiesUtils.loadProperties(propsUrl);

            this.dbConnect = config.getProperty(DATABASE_CONNECTTION);
            this.appCode = config.getProperty(APP_CODE);
            this.settingComcode = config.getProperty(SETTING_COMCODE);
            this.settingBusarea = config.getProperty(SETTING_BUSAREA);
            this.csvItem = config.getProperty(CSV_ITEM_PATH);
            this.csvOutputPath = config.getProperty(CSV_OUTPUT_PATH);
            this.headerText = config.getProperty(SETTING_HEADERTEXT);
            this.dummycode1 = config.getProperty(SETTING_DUMMYCODE1);
            this.dummycode2 = config.getProperty(SETTING_DUMMYCODE2);

        } catch (IOException e) {
            System.out.println("Error SapProp PropertiesFile" + e.toString());
        }

    }


    public String getDbConnect() {
        return dbConnect;
    }

    public String getAppCode() {
        return appCode;
    }

    public String getSettingComcode() {
        return settingComcode;
    }

    public String getSettingBusarea() {
        return settingBusarea;
    }

    public String getCsvItem() {
        return csvItem;
    }

    public String getCsvOutputPath() {
        return csvOutputPath;
    }

    public String getHeaderText() {
        return headerText;
    }

    public String getDummycode1() {
        return dummycode1;
    }

    public String getDummycode2() {
        return dummycode2;
    }

   
}
