package jp.co.nec.flowlites.usercode;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import jp.co.nec.flowlites.entity.HanyoEntity;
import jp.co.nec.flowlites.entity.SosikiEntity;
import jp.co.nec.flowlites.entity.SyainBaseEntity;
import jp.co.nec.flowlites.usercode.dao.EmployeeDao;
import jp.co.nec.flowlites.usercode.dao.PlacementDao;
import jp.co.nec.flowlites.usercode.dao.RouteDao;

/**
 * 承認関連処理
 */
public class USER_SYONIN implements UserCodeSyonin {

    public void afterCheckSinseiData(FunctionFlgs functionFlgs, UserInfo user,
            SinseiData sinseiData, RouteData routeData, Map params, Map tools)
            throws Exception {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date) + " USER_SYONIN afterCheckSinseiData " + user.getKanaSimei());

    }

    public void afterInitSyonin(FunctionFlgs functionFlgs, UserInfo user,
            SinseiData sinseiData, RouteData routeData, Map params, Map tools)
            throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date) + " USER_SYONIN afterInitSyonin " + user.getKanaSimei());
    }

    public void afterKyakka(FunctionFlgs functionFlgs, UserInfo user,
            SinseiData sinseiData, RouteData routeData, Map params, Map tools)
            throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date) + "--> USER_SYONIN afterKyakka " + user.getKanaSimei());

    }

    public void afterSaveSyonin(FunctionFlgs functionFlgs, UserInfo user,
            SinseiData sinseiData, RouteData routeData, Map params, Map tools)
            throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date) + " USER_SYONIN afterSaveSyonin " + user.getKanaSimei());
    }

    public void afterSyonin(FunctionFlgs functionFlgs, UserInfo user,
            SinseiData sinseiData, RouteData routeData, Map params, Map tools)
            throws Exception {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date) + " USER_SYONIN afterSyonin " + user.getKanaSimei());

    }

    public void beforeCheckSinseiData(FunctionFlgs functionFlgs, UserInfo user,
            SinseiData sinseiData, RouteData routeData, Map params, Map tools)
            throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date) + " USER_SYONIN beforeCheckSinseiData " + user.getKanaSimei());
        System.out.println(dateFormat.format(date) + " sinseiData.getHead().getCurrentStepNo() " + sinseiData.getHead().getCurrentStepNo());

    }

    public void beforeInitSyonin(FunctionFlgs functionFlgs, UserInfo user,
            SinseiData sinseiData, RouteData routeData, Map params, Map tools)
            throws Exception {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date) + " USER_SYONIN beforeInitSyonin " + user.getKanaSimei());
    }

    public void beforeKyakka(FunctionFlgs functionFlgs, UserInfo user,
            SinseiData sinseiData, RouteData routeData, Map params, Map tools)
            throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date) + " USER_SYONIN beforeKyakka " + user.getKanaSimei());

        System.out.println(dateFormat.format(date) + " USER_SYONIN sinseiData getCurrentStepNo " + sinseiData.getHead().getCurrentStepNo());
    }

    public void beforeSaveSyonin(FunctionFlgs functionFlgs, UserInfo user,
            SinseiData sinseiData, RouteData routeData, Map params, Map tools)
            throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date) + " USER_SYONIN beforeSaveSyonin " + user.getKanaSimei());

    }

    public void beforeSyonin(FunctionFlgs functionFlgs, UserInfo user,
            SinseiData sinseiData, RouteData routeData, Map params, Map tools)
            throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date) + " USER_SYONIN beforeSyonin " + user.getKanaSimei());
    }

    public boolean checkGyo(FunctionFlgs functionFlgs, UserInfo user,
            SinseiData sinseiData, RouteData routeData, Map params, Map tools)
            throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date) + " USER_SYONIN checkGyo " + user.getKanaSimei());

        return false;
    }

    public void routeCheck(FunctionFlgs functionFlgs, UserInfo user,
            SinseiData sinseiData, RouteData routeData, Map params, Map tools)
            throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date) + " USER_SYONIN routeCheck " + user.getKanaSimei());

    }

    public void routeUpdate(FunctionFlgs functionFlgs, UserInfo user,
            SinseiData sinseiData, RouteData routeData, Map params, Map tools)
            throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date) + " USER_SYONIN routeUpdate " + user.getKanaSimei());

        // =============================================== 
        System.out.println(dateFormat.format(date) + " NOW ---> getCurrentStepNo " + sinseiData.getHead().getCurrentStepNo());
        RouteDao routeDao = new RouteDao();
        EmployeeDao employeeDao = new EmployeeDao();
        PlacementDao placementDao = new PlacementDao();
        int stepNo = sinseiData.getHead().getCurrentStepNo() + 1;
        System.out.println("getSyoruiId : " + sinseiData.getHead().getSyoruiId());
        System.out.println("stepNo : " + stepNo);
        List<HanyoEntity> route = routeDao.getRoute(tools, sinseiData.getHead().getSyoruiId(), stepNo);
        System.out.println("route.size : " + route.size());
        if (route.size() > 0) {
            String Company = sinseiData.getBody().getChrData(3).getValue();
            System.out.println("Company : " + Company);
            String Group = sinseiData.getBody().getChrData(8).getValue();
            System.out.println("Group : " + Group);
            List<SyainBaseEntity> emps = routeDao.getEmployee(tools, Group, Company);
            for (SyainBaseEntity emp : emps) {
                System.out.println("Group : " + Group);
                SyainBaseEntity em = employeeDao.getById(tools, emp.getSYAIN_NO());
                SosikiEntity place = placementDao.getByEmp(tools, emp.getSYAIN_NO());
                System.out.println("em : " + em.getSYAIN_NO());
                // add empoyee
                Float FloatRouteStep = Float.parseFloat(route.get(0).getCHR2());
                int intStep = Math.round(FloatRouteStep);
                StepData stepData = routeData.getStepData(intStep);
                BunkiData bunkiData = stepData.addBunki(0);
                Syoninsya syoninsya = new Syoninsya(em.getSYAIN_NO(), em.getSYAIN_NO(), em.getSYAIN_BETUMEI(), emp.getSYAIN_BETUMEI(), em.getSYAIN_NO() + "_5", place.getSOSIKI_NAME() + "", em.getSYAIN_NO() + "_7", 1);
                bunkiData.addSyoninsya(syoninsya);
            }
        }

        // =============================================== 
    }

    public void torikesi(FunctionFlgs functionFlgs, UserInfo user,
            SinseiData sinseiData, RouteData routeData, Map params, Map tools)
            throws Exception {

    }

}
